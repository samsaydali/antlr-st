grammar Module;

module: entities '|' actions;

entities: '[entities:]' entity_def;

entity_def: TEXT;

actions: '[actions:]' TEXT;

fragment DIGIT : [0-9];
fragment LETTER : [A-Za-z];
fragment CAP_LETTER : [A-Z];

TEXT : LETTER+;
CRLF : '\r'? '\n' | '\r';