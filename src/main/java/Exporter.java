import model.LogEntry;
import org.stringtemplate.v4.ST;

public class Exporter {

    private LogListener listener;

    public Exporter(LogListener listener) {
        this.listener = listener;
    }

    public String export() {
        StringBuilder builder = new StringBuilder();
        listener.getEntries().forEach(e -> builder.append(logEntry(e)).append('\n'));
        return builder.toString();
    }

    private String logEntry(LogEntry entry) {
        ST template = new ST("$e.level$ $e.message$", '$', '$');
        template.add("e", entry);
        return template.render();
    }
}
