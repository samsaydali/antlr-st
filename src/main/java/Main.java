import gen.LogLexer;
import gen.LogParser;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.stringtemplate.v4.ST;
import org.stringtemplate.v4.STGroup;
import org.stringtemplate.v4.STGroupDir;

public class Main {
    public static void main(String[] args) {
        STGroup stGroup = new STGroupDir("mail");
        ST st = stGroup.getInstanceOf("spam_419");
        st.add("from_addr", "some@mail.com");
        st.add("to_addr", "to@mail.com");
        st.add("from", "some");
        st.add("to", "to");
        System.out.println(st.render());
    }

    public static void log() {
        String logs = "2018-May-05 14:20:18 INFO some error occurred\n" +
                "2018-May-05 14:20:19 INFO yet another error\n" +
                "2018-May-05 14:20:20 INFO some method started\n" +
                "2018-May-05 14:20:21 DEBUG another method started\n" +
                "2018-May-05 14:20:21 DEBUG entering awesome method\n" +
                "2018-May-05 14:20:24 ERROR Bad thing happened\n";

        LogLexer logLexer = new LogLexer(CharStreams.fromString(logs));

        CommonTokenStream tokens = new CommonTokenStream(logLexer);


        LogParser parser = new LogParser(tokens);
        ParseTree tree = parser.log();

        ParseTreeWalker walker = new ParseTreeWalker();
        LogListener listener= new LogListener();

        walker.walk(listener, tree);

        Exporter exporter = new Exporter(listener);
        System.out.println(exporter.export());
    }
}
